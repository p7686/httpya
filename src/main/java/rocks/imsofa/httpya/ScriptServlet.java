/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package rocks.imsofa.httpya;

import java.io.File;
import java.io.IOException;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.io.FileUtils;

/**
 *
 * @author USER
 */
public class ScriptServlet extends HttpServlet{
    protected static String servletDir=null;
    private void process(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            ScriptEngineManager factory = new ScriptEngineManager();
            ScriptEngine engine = factory.getEngineByName("groovy");
            engine.put("request", req);
            engine.put("response", resp);
            engine.put("session", req.getSession());
            engine.put("application", req.getServletContext());
            engine.put("out", resp.getWriter());
            String uri=req.getRequestURI();
            String fileName=uri.substring(uri.lastIndexOf("/")+1);
            File scriptRoot=new File(servletDir);
            File groovyFile=new File(scriptRoot, fileName+".groovy");
            engine.eval(FileUtils.readFileToString(groovyFile, "utf-8"));
        } catch (ScriptException ex) {
            ex.printStackTrace();
        }
    }
    
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        process(req, resp);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        process(req, resp);
    }
    
}
