/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */

package rocks.imsofa.httpya;

import java.io.File;
import net.sourceforge.argparse4j.ArgumentParsers;
import net.sourceforge.argparse4j.inf.ArgumentParser;
import net.sourceforge.argparse4j.inf.Namespace;
import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.DefaultHandler;
import org.eclipse.jetty.server.handler.HandlerList;
import org.eclipse.jetty.server.handler.ResourceHandler;
import org.eclipse.jetty.servlet.ServletContextHandler;

/**
 *
 * @author lendle
 */
public class HttpYa {

    public static void main(String[] args) throws Exception {
        ArgumentParser parser = ArgumentParsers.newFor("ya").addHelp(true).build()
                .description("A simple plain http server.");
        parser.addArgument("-p", "--port").dest("port").help("port number, default to 8080").type(Integer.class).metavar("N").setDefault(8080);
        parser.addArgument("-d", "--dir").dest("dir").help("directory, default to current").type(String.class).metavar("D").setDefault(".");
        parser.addArgument("-s", "--servlet_dir").dest("servlet_dir").help("directory to put servlet scripts, default to servlets/").type(String.class).metavar("S").setDefault("servlets");
        Namespace ns=null;
        try{
            ns=parser.parseArgs(args);
        }catch(Throwable e){
            System.exit(0);
        }
        int port=ns.getInt("port");
        String dir=ns.getString("dir");
        Server server = new Server(port);
        ServletContextHandler servletContextHandler = new ServletContextHandler(ServletContextHandler.SESSIONS);
        servletContextHandler.setContextPath("/servlet");
        servletContextHandler.addServlet(ScriptServlet.class, "/");
        ScriptServlet.servletDir=ns.getString("servlet_dir");
        ResourceHandler resource_handler = new ResourceHandler();
        resource_handler.setResourceBase(new File(dir).getCanonicalPath());
        resource_handler.setDirAllowed(true);
        resource_handler.setDirectoriesListed(true);
        HandlerList handlers = new HandlerList();
        handlers.setHandlers(new Handler[] { servletContextHandler, resource_handler, new DefaultHandler() });
        server.setHandler(handlers);
        server.start();
        server.join();
    }
}
